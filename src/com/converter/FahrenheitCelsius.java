package com.converter;

import java.util.Scanner;

/**
 * Fahrenheit-celcius converter. It is part of a certification process from this course :
 * https://openclassrooms.com/courses/apprenez-a-programmer-en-java
 * 
 * Find below the link of the wording (french): 
 * https://openclassrooms.com/courses/apprenez-a-programmer-en-java/tp-conversion-celsius-fahrenheit
 *
 * @author 	Fabien Huitelec
 * @version	1.0
 */
public class FahrenheitCelsius {
	// Util
	private static Scanner sc;
	// Description text
	private static final String
	DESCRIPTION 			= "CONVERTISSEUR DEGRS CELSIUS ET DEGR�S FAHRENHEIT" + "\n"
							+ "------------------------------------------------",
	TEMPERATURE_DESCRIPTION = "Temp�rature � convertir :",

	CONVERTER_DESCRIPTION 	= "1 - Convertisseur Celsius - Farenheit" + "\n"
							+ "2 - Convertisseur Farenheit - Celsius",
	CONTINUE_DESCRIPTION	= "Souhaitez-vous convertir une autre temp�rature ? (O/N)",
	GOODBYE					= "Au revoir !";	
	
	/**
	 * Asks for conversion way,
	 * for the temperature,
	 * displays the converted temperature and
	 * asks if it has to continue over again.
	 * 
	 * @param args			Main args
	 * @throws Exception	In case the user choice function return is not correct
	 */
	public static void main(String[] args) throws Exception
	{
		// Initialisation
		sc = new Scanner(System.in);
		System.out.println(DESCRIPTION);
		
		// While the user chooses to stop
		do
		{
			// Do the conversion...
			switch (getConverterEntry())
			{
			// ... Celsius to Fahreneit
			case 1:
				converterCelsiusToFahrenheit(getTemperatureEntry());
				break;
			// ... Fahreneit to Celsius 
			case 2:
				converterFahrenheitToCelsius(getTemperatureEntry());
				break;
			default:
				throw new Exception("There must be a choice : 1 or 2.");
			}
		} while ( getContinueEntry() );

		// Termination
		System.out.println(GOODBYE);
		sc.close();
	}

	/**
	 * @return The temperature to be converted well formated.
	 */
	public static double getTemperatureEntry() 
	{
		double res = -1.0d;
		String entry = "";

		while ( entry.equals("") )
		{
			try 
			{
				System.out.println(TEMPERATURE_DESCRIPTION);
				entry = sc.nextLine();
				res = Double.parseDouble(entry);
			}
			catch (NumberFormatException e)
			{
				entry = "";
			}
		}
	  
		return res;
	}
	
	/**
	 * @return The user converter. Expected to be 1 or 2.
	 */
	public static int getConverterEntry() 
	{
		String entry = "";
		
		while ( entry == "" )
		{
			System.out.println(CONVERTER_DESCRIPTION);
			entry = sc.nextLine();
			
			if (!entry.equals("1") && !entry.equals("2"))
				entry = "";
		}
		
		System.out.println(Integer.parseInt(entry));
		
		return Integer.parseInt(entry);
	}
	
	/**
	 * @return The user choice to rather continue or not
	 */
	public static boolean getContinueEntry()
	{
		String entry = "";
		
		while ( entry.equals("") )
		{
			System.out.println(CONTINUE_DESCRIPTION);
			entry = sc.nextLine().toUpperCase();
			
			if (entry.equals("O"))
				return true;
			else if (entry.equals("N"))
				return false;
			else
				entry = "";
		}
		
		return false;
	}
	
	/**
	 * Prints the Celsius to Fahrenheit conversion.
	 * 
	 * @param celsius Celsiut temperature
	 */
	public static void converterCelsiusToFahrenheit(double celsius)
	{
		double fahrenheit = 9.0d / 5.0d * celsius + 32.0d;
		System.out.println(round(celsius, 2) + "�C correspond � : " + round(fahrenheit, 2) + "�F.");
	}
	
	/**
	 * Prints the Fahrenheit to Celsius conversion.
	 * 
	 * @param fahrenheit Fahrenheit temperature	
	 */
	public static void converterFahrenheitToCelsius(double fahrenheit)
	{
		double celsius = (fahrenheit - 32.0d) * 5 / 9;
		System.out.println(round(fahrenheit, 2) + "�F correspond � : " + round(celsius, 2) + "�C.");
	}
	
	/**
	 * Rounds a number A to B decimal places.
	 * Used in this class but not the best solution (the one being the built-in setRoundingMode of DecimalFormat).
	 * 
	 * @param A	Number being rounded
	 * @param B Decimal places
	 * 
	 * @return	Rounded number
	 * 
	 * @deprecated
	 * @see		<a href="http://docs.oracle.com/javase/6/docs/api/java/text/DecimalFormat.html#setRoundingMode%28java.math.RoundingMode%29">DecimalFormat</a>
	 */
	public static double round(double A, int B) {
	  return (double) ( (int) (A * Math.pow(10, B) + .5)) / Math.pow(10, B);
	}
}