#Contributing
This repository has only a self-learning goal.<br /> In this context, no contribution is expected. However, if you have any suggestion about code writing, best practices, etc. please open a ticket or submit a pull request.
