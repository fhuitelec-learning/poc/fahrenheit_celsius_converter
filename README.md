# Fahrenheit-celsius converter
> Stable

This repository is a little knowledge base with self-learning goal:<br />
Two-way Fahrenheit-Celcius converter written in Java. It is part of a certification process from this course :
https://openclassrooms.com/courses/apprenez-a-programmer-en-java



Find below the link of the wording (french):
https://openclassrooms.com/courses/apprenez-a-programmer-en-java/tp-conversion-celsius-fahrenheit